import React, { useState } from 'react';
import './VendingMachine.css';
import ListItemComponent from '../components/ListItemComponent';

const VendingMachine = () => {
    const [money,setMoney] = useState(0)
    const [item] = useState([
        {name : "Biskuit", price : 6000, qty : 0, image: 'biskuit.jpg' },
        {name : "Chips", price : 8000, qty : 1, image: 'chips.jpg' },
        {name : "Oreo", price : 10000, qty : 7, image: 'oreo.jpg' },
        {name : "Tango", price : 12000, qty : 9, image: 'tango.jpg' },
        {name : "Coklat", price : 15000, qty : 4, image: 'coklat.jpg' }
    ])
    const [yourItem,setYourItem] = useState([])
    const [change,setChange] = useState({})
    let rule = [50000,20000,10000,5000,2000]
    let [isTaken,setIsTaken] = useState(false)
    const handleSubmit = (event) => {
        event.preventDefault()
        let inMoney = parseInt(event.target.money.value)
        if(!rule.includes(inMoney)){
            alert("Please only insert 2000, 5000, 10000, 20000 or 50000 to the machine!")
        }else{
            inMoney = money + inMoney
            setMoney(inMoney)
            setChange({})
            setIsTaken(false)
        }
        
    }
    const handlePick = (event, param) => {
        if(money == 0){
            alert("Please insert your money first!")
        }
        else if(param.el.price > money){
            alert("Your Money Not Enough!")
        }
        else{
            let remain = money - param.el.price
            setMoney(remain)
            setYourItem([...yourItem, param.el])
            let indexItem = item.indexOf(param.el)
            console.log(indexItem)
            item[indexItem].qty--

        }
    }
    const takeRemain = (event) => {
        let arrayChange = []
        let tempMoney = money
        while(tempMoney != 0){
            rule.every(rl => {
                if(tempMoney >= rl){
                    arrayChange.push(rl)
                    tempMoney -= rl
                    return false
                }else{
                    return true
                }
            });
        }
        let counts = {}
        arrayChange.forEach((x) => { counts[x.toString()] = (counts[x.toString()]||0) + 1 });
        setChange(counts)
        setMoney(0)
        setIsTaken(true)
    }
        
    return (
        <div>
            <h2>Money : {money}</h2>
            <form onSubmit={handleSubmit}>
                <label>Input Your Money :  </label>
                <input type="number" name='money' min='0' />
                <input className="buttonPrimary" type="submit" value="Submit" />
            </form>
            <h2>Snack</h2>
            <div className='cards' >
            {
                item.map((el,index) => {
                    return(
                        <ListItemComponent el={el} key={index} handlePick={handlePick}></ListItemComponent>
                    )
                })
            }
            </div>
            <h2>Your Item :</h2>
            <div className='cards' >
            {
                yourItem.map((el,index) => {
                    return(
                        <ListItemComponent el={el} key={index} handlePick={handlePick}></ListItemComponent>
                    )
                })
            }
            </div>
            <button className="buttonDanger" onClick={takeRemain}>Take Remaining Money</button>
            <h2>Your Change : </h2>
            {(() => {
                if(isTaken){
                    return(
                        rule.map(el => {
                            if(change[el]){
                                return(
                                    <h4 key={el} >{el} x {change[el]}</h4>
                                ) 
                            }
                               
                        })
                    )
                    
                }
            })()}
            
        </div>
    )
}

export default VendingMachine;
