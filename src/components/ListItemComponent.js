import React from 'react';

const ListItemComponent = (props) => {
    const {el,handlePick} = props
    
    const styleButton = {
        backgroundImage: `url(${require(`../images/${el.image}`)})`, 
        backgroundRepeat: "no-repeat",
        backgroundSize: "100% 100%",
        backgroundColor: "grey",
        width:"200px",
        height:"100px",
        borderRadius:"10px",
        marginLeft:"10px",
        opacity: el.qty < 1 ? '0.5' : '1'
    }
    return (
        <div>
            <button
            style={styleButton} 
            onClick={event =>handlePick(event,{el})}
            disabled = { el.qty < 1}
            ></button>
        </div>     
    )
}

export default ListItemComponent;
