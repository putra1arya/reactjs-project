
import './App.css';
import VendingMachine from './pages/VendingMachine';

function App() {
  return (
    <div className="App">
      <h1>Vending Machine</h1>
      <VendingMachine></VendingMachine>
    </div>
  );
}

export default App;
